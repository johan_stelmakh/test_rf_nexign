*** Settings ***
Library     Collections
Library     RequestsLibrary

*** Variables ***
${good_imsi_value}=     h_
${bad_imsi_value}=      h+

*** Test Cases ***
Positive get-request
	Create Session 	    webserver 	        http://localhost:8080
	&{headers}=         Create Dictionary   imsi=${good_imsi_value}
	${resp}= 	        Get Request 	    webserver 	                /       headers=${headers}
	Should Be Equal As Strings 	            ${resp.status_code} 	    200

Negative get-request with bad imsi header
	Create Session 	    webserver 	        http://localhost:8080
	&{headers}=         Create Dictionary   imsi=${bad_imsi_value}
	${resp}= 	        Get Request 	    webserver 	                /       headers=${headers}
	Should Be Equal As Strings 	            ${resp.status_code} 	    400

Negative get-request without imsi header
	Create Session 	    webserver 	        http://localhost:8080
	${resp}= 	        Get Request 	    webserver 	                /
	Should Be Equal As Strings 	            ${resp.status_code} 	    400
