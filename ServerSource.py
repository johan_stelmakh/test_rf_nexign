from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import re

PORT_NUMBER = 8080
""" Create a child class that is responsible for processing requests to the server """
class myHandler(BaseHTTPRequestHandler):
    """ Handle get-requests """
    def do_GET(self):
        pattern = re.compile("^[A-Za-z0-9_]+$")
        print(self.headers.getheader("imsi"))
        """ If the "imsi" header exists and satisfies the regular expression, return code 200, otherwise 400 """
        if self.headers.getheader("imsi") is not None and pattern.match(self.headers.getheader("imsi")):
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
        else:
            self.send_response(400)
            self.end_headers()
        return


try:
    """ Create a web server and a query handler """
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ', PORT_NUMBER

    """ Waiting for requests before interruption """
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
