Testing Simple Server with Robot Framework
Requires 2.7 Python interpreter and robot framework with RequestsLibrary.

It is not clear from the condition which regular expression should be chosen
(to which class the empty imsi value should be assigned), so I just chose one of them randomly:
"^[A-Za-z0-9_]+$" or "^[A-Za-z0-9_]*$"